const mongoose = require('mongoose')

const roomschema = mongoose.Schema({
    user:{
        type:mongoose.Types.ObjectId,
        ref:"User"
    },
    rooms:{
        type:[String],
        unique:true
    },
    createdAt:{
        type:Date,
        default:Date.now()
    }
})

module.exports = mongoose.model('room',roomschema)