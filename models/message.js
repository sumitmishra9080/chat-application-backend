const mongoose = require('mongoose')

const messageschema = mongoose.Schema({
    from:{
        type:String
    },
    to:{
        type:String
    },
    content:{
        type:String
    },
    createdAt:{
        type:Date,
        default:Date.now()
    }
})

module.exports = mongoose.model('messages',messageschema)