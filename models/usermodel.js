const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const crypto = require('crypto')
const { log } = require('console')

const userschema = mongoose.Schema({
    name:{
        type:String,
        unique:true,
        maxlength:50,
    },
    password:{
        type:String,
        required:true,
        minlenth:6,
        select:false
    },
    email:{
        type:String,
        unique:true,
        maxlength:100,
        match:[/^[\w.-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,}$/,"wrong mail"]
    },
    isonline:{
        type:Boolean
    },
    resetPasswordToken:String,
    resetPasswordExpire:Date,
    createdAt:{
        type:Date,
        default:Date.now()
    }
})


userschema.pre('save',async function(next){
        if(!this.isModified('password')){
            next()
        }
        const salt =await bcrypt.genSalt(10);
        this.password = await bcrypt.hash(this.password,salt)
        console.log("hashing password ...")
   

})

userschema.methods.resetpasswordtoken = async function(){
    const rand =crypto.randomBytes(20).toString('hex')
    this.resetPasswordToken =await crypto.createHash('sha256').update(rand).digest('hex')
    this.resetPasswordExpire = Date.now() + 30 * 60 * 1000;
    return this.resetPasswordToken
}

module.exports = mongoose.model('User',userschema)