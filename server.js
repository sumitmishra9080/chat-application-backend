const express = require('express');
const dotenv = require('dotenv')
const connectdatabase = require('./config/db')
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const path = require('path')
const bodyparser = require('body-parser')
const User = require('./models/usermodel')
const messages = require('./models/message')
const jwt = require('jsonwebtoken')
const roommodel = require('./models/rooms');
const { log } = require('console');
const { Socket } = require('net');

dotenv.config({ path: './config/config.env' })

connectdatabase();
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'ejs')

app.use(express.json())

const logger = (req, res, next) => {
    console.log(`${req.method} ${req.protocol}://${req.hostname}:${process.env.PORT}${req.originalUrl}`)
    next()
}
app.use(logger)

app.use(express.urlencoded());
// app.get('/chats',async (req,res,next)=>{
//     const decodedsocketids = decodersockets(socketidwithtoken)
//     console.log(decodedsocketids);
//     res.render("chats.ejs",{socketids:decodedsocketids})
// })
app.get('/', function (request, response, next) {

    response.send(`
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<div class="container">
        <h1 class="text-center mt-3 mb-3">Submit Form Data in Node.js</h1>
        <div class="card">
        <div class="card-header">Sample Form</div>
        <div class="card-body">
        <form method="POST" action="/">
        <div class="mb-3">
							<label>Email</label>
							<input type="text" name="email" id="email" class="form-control" />
						</div>
						<div class="mb-3">
                        <label>password</label>
                        <input type="text" name="password" id="password" class="form-control" />
						</div>
		                <div class="mb-3">
                        <input type="submit" id="submit_button" class="btn btn-primary"/>
		                </div>
                        </form>
                        </div>
			</div>
		</div>
	`);


});

app.post('/', async function (req, res, next) {
    console.log(req.body);
    const { email, password } = req.body
    const alluser = await User.aggregate([{ $group: { _id: null, names: { $push: "$name" } } }, { $project: { _id: 0 } }])
    const user = await User.findOne({ email: email }).select('+password')
    console.log("here", alluser);
    if (!user) {
        return res.redirect('/')
    }
    if (password !== user.password) {
        console.log("here", user.password);
        return res.redirect('/')
    }

    const rooms = await roommodel.find({ user: user._id }, { rooms: 1, _id: 0 })
    console.log(rooms);

    const token = jwt.sign({ user }, process.env.JWTSECRETKEY)
    if (!token) {
        return res.redirect('/')
    }

    res.render('index.ejs', { user, token: token, listofuser: alluser, rooms: rooms, sockets: socketidwithtoken })

});


app.post('/room/:username/:room', async (req, res, next) => {
    const user = await User.findOne({ name: req.params.username })
    const room = await roommodel.create({ user: user._id, rooms: req.params.room })
    res.status(201).json({ room })
})

var socketidwithtoken = [];
var names = []

const connectedSocketIds = [];

io.on('connection', (socket) => {
  console.log(`Socket connected: ${socket.id}`);
     const idwithtoken = { socketid: socket.id, token: socket.handshake.auth.token }
      console.log(socket.rooms);
      let decodedsocketids = decodersockets(idwithtoken)
      
  connectedSocketIds.push(decodedsocketids);

  io.emit('allSocketIds', connectedSocketIds);

  socket.on('disconnect', () => {
    console.log(`Socket disconnected: ${socket.id}`);
    console.log(connectedSocketIds);
    const index = findindex(connectedSocketIds,socket.id)
    console.log("answer is ",index);
    // const index = connectedSocketIds.indexOf(socket.id);
    if (index !== -1) {
      connectedSocketIds.splice(index, 1);
    }
})

    
   
    //   console.log("realanswer is",socketidwithtoken)

    // if (!socketidwithtoken.length) {
    //     console.log("1jbadyusicfvladuyjfdohadgsuvcasbyeuogbhduidkcbcdwyvcqiwncuergfgfu");
    //     socketidwithtoken.push(decodedsocketids)
        
    // }
    // else {
    //     socketidwithtoken.forEach(element => {
    //         console.log("11",element.token.user.name,decodedsocketids.token.user.name);
    //         if (element.token=== decodedsocketids.token) {
    //             Object.assign(element, decodedsocketids);
    //             console.log("hello",decodedsocketids,element);
    //         }
    //         // else if (!socketidwithtoken.includes(element)){
    //             //     console.log("yse persent");
    //             //     socketidwithtoken.push(decodedsocketids)
                    
    //             // }
    //             else if (!isTokenAlreadyPresent(socketidwithtoken, decodedsocketids.token)) {
    //                 socketidwithtoken.push(decodedsocketids);
    //                 console.log("new token added:", decodedsocketids);
    //             }
    //             console.log(socketidwithtoken.includes(element));
    //         });
        
    //     }
    //     function isTokenAlreadyPresent(array, token) {
    //         return array.some(element => element.token === token);
    //     }
    //     console.log("ansnwer", socketidwithtoken);
    //     io.emit('socketinfo',socketidwithtoken)
                const me = generateinfofromtoken(socket.handshake.auth.token)
                // console.log(me);
                if (me) {
        socket.on('custom-message', async (data) => {
            console.log(data);
            // const me = generateinfofromtoken(socket.handshake.auth.token)
            // await messages.create({ from: me.user.name, to: data.receiver, content: data.message })
            // socket.broadcast.emit('receive-message', { msg: data.message })
            console.log("vimp", socket.rooms);
            socket.to(data.room).emit('receive-message', { msg: data.message })
        })
    }
    socket.on('joinroom', async (room, token) => {
        console.log(room, token);
        const info = generateinfofromtoken(token)
        var addroom = await roommodel.findOneAndUpdate({ user: info.user._id }, { $addToSet: { rooms: room } })

        if (!addroom) {
            addroom = await roommodel.create({ user: info.user._id, rooms: room })

        }
        addroom.rooms.forEach(element => {
            socket.join(element)
        });

    })
})

http.listen(3000, () => {
    console.log(`Socket.IO server running at http://localhost:3000`);
});

function findindex(arr,key){
    for (let i = 0;i<arr.length;i++){
        console.log(arr[i].socketid,key);
        if (arr[i].socketid === key){
            return i;
        }
    }
}

function decodersockets(obj) {
    const info = generateinfofromtoken(obj.token)
    obj.token = info
    return obj
}
function generateinfofromtoken(token) {
    return jwt.verify(token, process.env.JWTSECRETKEY)
}
